var express = require('express'),
	curl = require('curlrequest'),
	http = require('http'),
    connect = require('connect'),
    twilio = require('twilio'),
    _ = require('underscore'),
    moment = require('moment'),
    crypto = require('crypto'),
    sendgrid = require('sendgrid')('wolframcreative','LifthsP1'),
    analytics = require('analytics-node'),
    client = new twilio.RestClient('AC54aad2ea88b0388407c28056cc4e422e', 'a4c3ad816aef1cb84dd32efb831b1c01');

    analytics.init({ secret: 'myw6js0asw3q24swrjxl' });

    var databaseUrl = (process.env.NODE_ENV == "production") ? "wolframcreative:LifthsP!@paulo.mongohq.com:10088/n8" : "etanlubeck:LifthsP!@localhost/n8_local"; // "username:password@example.com/mydb"

    var collections = ["Articles","Users","Platforms","Comments"];
    var db = require("mongojs").connect(databaseUrl, collections);

    var app = express();

var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, api_token, project_id');
  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.send(200);
  } else {
    next();
  }
};

app.configure(function(){
    app.set('port', process.env.PORT || 3000);
    app.use(allowCrossDomain);
    app.use('/img', express.static(__dirname + '/www/img'));
    app.use('/js', express.static(__dirname + '/www/js'));
    app.use('/css', express.static(__dirname + '/www/css'));
    app.use('/downloads', express.static(__dirname + '/www/downloads'));
    app.use('/tpl', express.static(__dirname + '/www/tpl'));
    app.use(express.favicon(__dirname + '/www/favicon.ico'));
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.cookieParser());
    app.use(express.methodOverride());
    app.set('views', __dirname + '/www');
    app.engine('html', require('ejs').renderFile);
});

app.get('/google98b653ebf9aa4e44.html', function (req, res) {
    res.render('google98b653ebf9aa4e44.html');
});


app.get('/*', function (req, res) {
    res.render('index.html');
});

app.get('/api/get_articles', function(req, res) {
    db.Articles.find(function(data){
        console.log(arguments);
        if(data) {
            res.send(data);
        }
    });
});

//app.get('/downloads/tableproject+.safariextz', function (req, res){
//    res.download('/downloads/tableproject+.safariextz');
//});

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});