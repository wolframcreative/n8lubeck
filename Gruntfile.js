var path = require('path'),
	fs = require('fs'),
	stylesheetsDir = 'assets/sass',
	dotenv = require('dotenv')().load(),
	pkg = require('./package.json'),
	production = (process.env.NODE_ENV === "production") ? true : false,
	Handlebars = require('handlebars'),
	jsDir = 'assets/js/';

// need to install hbs and pass stuff to template in a custom task

module.exports = function(grunt) {
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat : {
			options : {
				banner : "/* <%=pkg.name%> - <%=pkg.version%> " + (+new Date()) + " */\n\n\n" // im a hack
			},
			dist: {
				src : [
					// assets
					jsDir + "libs/underscore-min.js",
					jsDir + "libs/backbone-min.js",
					jsDir + "libs/handlebars.js",
					jsDir + "libs/moment.js",
					jsDir + "templates.js",
					jsDir + "util/*.js",
					// the actual app files
					"app/models/app.js",
					"app/views/BaseView.js",
					"app/views/AppView.js",
					"app/models/*.js",
					"app/collections/*.js",
					"app/views/pages/*.js",
					"app/router.js",
					"app/app.js"
				],
				dest : 'www/js/app.js'
			}
		},
		handlebars: {
			compile: {
				options: {
					namespace: "__tmp",
					processName: function(filename) {
						return filename.replace('app/templates/', '').replace('.hbs', '');
					}
				},
				src: "app/templates/**/*.hbs",
				dest: "assets/js/templates.js",
				filter: function(filepath) {
					var filename = path.basename(filepath);
					// Exclude files that begin with '__' from being sent to the client,
					// i.e. __layout.hbs.
					return filename.slice(0, 2) !== '__';
				}
			}
		},
		watch: {
			templates: {
				files: 'app/templates/**/*.hbs',
				tasks: ['compile'],
				options: {
					interrupt: false
				}
			},
			app : {
				files : 'app/**/*.js',
				tasks: ['concat'],
				options : {
					interrupt : false
				}
			},
			assets : {
				files : 'assets/js/**/*.js',
				tasks: ['concat'],
				options : {
					interrupt : false
				}
			},
			stylesheets: {
        		files:  'assets/sass/**/*.scss',
        		tasks: ['compass'],
        		options: {
          			interrupt: false
        		}
      		}
		},

		concurrent : {
			target : {
				tasks : ["watch", "nodemon"],
				options: {
                    file : "server.js",
					logConcurrentOutput: true
				}
			}
		},
		compass: {                  
	      	dist: {                   
	        	options: {              
	          		sassDir: 'assets/sass',
	          		cssDir: 'www/css',
		          	imagesDir : 'www/img',
		          	httpImagesPath  : "/img",
		          	httpGeneratedImagesPath : "/img",
		          	outputStyle : (production) ? 'compressed' : "expanded",
		          	noLineComments : (production) ? true : false,
		          	force : false
	        	}
	      	}
	    },
         nodemon: {
            dev: {
                options: {
                    file: 'server.js',
                    watchedExtensions: ['js'],

                    delayTime: 1,
                    legacyWatch: true,
                    env: {
                        PORT: '1337'
                    },
                    cwd: __dirname
                }
            }
        }
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-handlebars');
	grunt.loadNpmTasks('grunt-concurrent');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-nodemon');

	grunt.registerTask(
		'layout', 
		'Precompiles index file for ' + 
		'better configurations',
		function ( ) { 
			var file = fs.readFileSync( 'app/templates/__layout.hbs' );
			var template = Handlebars.compile( file.toString() );
			var result = template( {
				DEBUG: process.env.DEBUG,
				config: {
					appName: pkg.name,
					version: pkg.version
				}
			} );

			fs.writeFileSync('./www/index.html', result );
			grunt.log.writeln('file /www/index.html created');
		}
	);




	grunt.registerTask('compile', ['layout', 'compass', 'handlebars', 'concat']);

	// Run the server and watch for file changes
	grunt.registerTask('server', ['compile', 'concurrent']);

	// Default task(s).
	grunt.registerTask('default', ['compile']);


};
