(function(){

	var 
	toHtml = function( $el ){
		var pseu = $('<div>').append( $el );
		return pseu.html();
	}
	// we can probably just take all of the ones from the other framework

	Handlebars.registerHelper("date", function ( ts ) {
		return new Handlebars.SafeString( 
			moment( ts, 'YYYY-MM-DD-HH-mm-ss').fromNow() 
		)
		//2013-07-20T16:10:54.761Z
	});

	Handlebars.registerHelper("is", function( value, comparator, options ){
		var ret = '';
		if( value === comparator ){
			ret += options.fn( this );
		}else{
			ret += options.inverse( this );
		}
		return ret;
	});


	Handlebars.registerHelper('json', function( obj ){
		return JSON.stringify( obj ); 
	});




	Handlebars.registerHelper('view', function(viewName, options ){

		var 
		View = app.getView( viewName ),
		html,
		parent = this._view || null;


		if ( View ) {
			var view = new View( options.hash );

			if ( parent ) {
				var parentView = app.getStoredView( parent );
				// attach to the child
				view.parentView = parentView;

				parentView.childViews = parentView.childViews || [];
				parentView.childViews.push( view );
			}

			view.render();
			// need to attach parent
			html = view.getHtml();
      		return new Handlebars.SafeString(html);
		}

	} );

	Handlebars.registerHelper('partial', function( partialName, payload, options ){

		var partial = __tmp[ partialName ];

		if( typeof partial === 'function' ){
			if( !options ){
				payload = {};
			}
			return partial( payload );
		}

	} );

}());