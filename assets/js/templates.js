this["__tmp"] = this["__tmp"] || {};

this["__tmp"]["404"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"row\">\n  	<h1>404</h1>\n  	<div class=\"small-6 small-centered columns\">\n	  	<p>DOH! Something got lost in your quest to find the end of the internet. <a href=\"/\">Go back?</a></p>\n  	</div>\n</div>\n";
  });

this["__tmp"]["bg/instagram"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<canvas id=\"ig_bg\"></canvas>";
  });

this["__tmp"]["header/base"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div id=\"header\">\n    <div id=\"logo\">\n        <img src=\"img/eta_logo.png\" title=\"Nate Lubeck | Javascript Ninja\">\n    </div>\n    <ul id=\"nav\">\n        <li>Code</li>\n        <li>Art</li>\n        <li>Life</li>\n        <li class=\"active\">Info</li>\n    </ul>\n    <div id=\"tools\">\n        <div id=\"leap-motion\"></div>\n        <div id=\"echo-cast\"></div>\n        <div id=\"mobile\">\n\n        </div>\n    </div>\n</div>";
  });

this["__tmp"]["landing"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing;


  options = {hash:{},data:data};
  stack2 = ((stack1 = helpers.partial || depth0.partial),stack1 ? stack1.call(depth0, "header/base", options) : helperMissing.call(depth0, "partial", "header/base", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n<div id=\"content\" class=\"row\">\n    <div class=\"ten columns mobile-four centered\">\n        <h1>Nate Lubeck</h1>\n        <h3>Web Developer/Serial Entrepreneur</h3>\n        <div id=\"info\">\n            <p><strong>Congratulations,</strong> you've caught me <u>red-handed</u>! I'm busy building applications.</p>\n            <ul>\n            Feel free to connect with me via:\n            <li>Twitter: <a href=\"http://www.twitter.com/etanlubeck\" target=\"_blank\">@etanlubeck</a></li>\n            <li>Dribbble: <a href=\"http://www.dribbble.com/etanlubeck\" target=\"_blank\">@etanlubeck</a> | GitHub: <a href=\"http://www.github.com/etanlubeck\" target=\"_blank\">@etanlubeck</a> | <a href=\"http://www.behance.net/etanlubeck\" target=\"_blank\">Behance</a></li>\n            <li>LinkedIn: <a href=\"http://www.linkedin.com/in/etanlubeck\" target=\"_blank\">www.linkedin.com/in/etanlubeck</a></li>\n            <li>Email: <a href=\"mailto:me@natelubeck.com?Subject=Hire%20Me\">me@natelubeck.com</a></li>\n\n            </ul>\n        </div>\n    </div>\n</div>\n\n";
  return buffer;
  });