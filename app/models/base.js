app.set( 'BaseModel', 
	Backbone.Model.extend({
		initialize : function( options ){
			this.store();
			if ( typeof this.postInitialize === 'function' ) {
				this.postInitialize( options );
			} 
		},
		store : function(){
			if ( !app.has( 'modelStore' ) ) {
				app.set( 'modelStore', {} );
			}

			var 
			id = this.get( this.idAttribute );
			store = app.get( 'modelStore' );

			// avoid storing `undefined`
			if ( id ) {
				store[ id ] = this;
			}

			app.set( 'modelStore', store );
		}
	})
)
