var app = app || {};

(function(){

	/** ###AppModel
	  * 
	  *	the model of the main app, its pretty much a backbone model
	  * but contains lots of goodies for registering views
	  * 
	  * Defaults : 
	  * 	api : depending on the enviroment it will choose to hit the live
	  * 	or devlopment api
	  *
	  */

	var AppModel = Backbone.Model.extend({
		_set : function( type, name, _this ){
			if ( !this.has( type ) ) {
				this.set( type, {} );
			}

			var 
			items = this.get( type ),
			item = items[ name ];

			if ( !item ) {
				items[ name ] = _this;
				this.set( type, items ); 
			}
		},
		_get : function( type, name ){
			type = type.toLowerCase() + 's';
			if ( this.has( type ) ) {
				return this.get( type )[ name ];
			}
			return null;
		},
		getStoredView : function( cid ){
			if ( this.has('viewStore') ) {
				var cache = this.get('viewStore');
				return cache[ cid ];
			}
			return null
		},
		cached : function( type, id ){
			type = type.toLowerCase() + 'Store';
			if( this.has( type ) ){
				var cache = this.get( type );
				return cache[ id ];
			}
			return null;
		},
		Item : function( type, item, base ){
			var baseName = 'Base' + type;

			if ( base || this.has( baseName ) ) {
				var 
				Base = base || this.get( baseName );
				id = item.id;

				if ( typeof id === 'string' ) {
					this._set( type.toLowerCase() + 's', id, Base.extend( item ) )
				}
			} 
		},

		// mapping functions

		Model : function( model, base ){
			this.Item( 'Model', model, base );
		},
		View : function( view, base ){
			this.Item( 'View', view, base );
		},
		Collection : function( model, base ){
			this.Item( 'Collection', model, base );
		},
		getModel : function( name ){
			return this._get( 'Model', name );
		},
		getView : function( name ){
			return this._get( 'View', name );
		},
		getCollection : function( name ){
			return this._get( 'Collection', name );
		},
		fetch : function( spec, options, callback ){
			var noopts;

			if( !callback && typeof options === 'function' ){
				callback = options;
				noopts = true;
			}

			if( spec.collection ){
				spec = spec.collection;

				if( spec.collection ){
					var 
					Collection = this.getCollection( spec.collection ),
					params = spec.params;


					if( Collection ){

						var collection = new Collection( );

						if( params.url ){
							collection.url = params.url;
						}

						collection.fetch({ 
							data : params, 
							success : function( collection, data ){
								// needs to pass back collection
								callback( null, collection, data );
							}, 
							error : function(  ){
								// just send back err
								callback( arguments[2] );
							} 
						});

					}else{
						callback( new Error('a valid collection must be specified') );
					}	

				}else{
					callback( new Error('a collection must be specified') );
				}

			}else if( spec.model ){
				//need to set this up
				var msg = 'cannot fetch for models at this time';

				if( typeof console === 'object' ){
					console.error( msg );
				}

				callback( new Error( msg ) );
			}
		}
	});

	_.extend( app, new AppModel( {} ) );

}());