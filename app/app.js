$(function( ){
	"use strict";
	//app.content = new app.AppView();
	app.Router = new app.Workspace();
	Backbone.history.start( { pushState : true } );	

	app.Router.on("loading", function(){
		app.set("loading", true);
		app.$body.addClass("loading");
	}, this)

	app.Router.on("done", function(){
		app.set("loading", false);
		app.$body.removeClass("loading");
		app.setState();
	}, this)


});