app.set( "BaseView" , Backbone.View.extend({
		// store view 
		initialize : function( options ){
			this.store();
			this._ts = +new Date();
			if( typeof this.postInitialize === "function" ) {
				this.postInitialize( options );
			}
		},
		render : function(){
			var payload, template;

			template = this.getTemplate();

			if( !template ){
				throw new Error(this.name + ": template \"" + this.getTemplateName() + "\" not found.");
			}

			payload = this.getTemplateData();
			payload = _.extend( payload || {} , { _view : this.cid } );


			this.$el.html( template( payload ) );

			if ( this.postRender ) {
				this.postRender();
			}
		},
		getPartial : function ( name ) {
			return __tmp[ name ];
		},
		setCollection : function( collection ){
			this.collection = collection;
			this.render();
		},
		setModel : function ( model ) {
			this.model = model;
			this.render();
		},
		store : function ( ) {
			if ( !app.has( 'viewStore' ) ) {
				app.set( 'viewStore', {} );
			}

			var store = app.get( 'viewStore' );
			// needs to be smarter
			store[ this.cid ] = this;

			app.set( 'viewStore', store );
		},
		getAttributes: function() {
		    var attributes = {};

		    if (this.id) {
		      	attributes.id = this.id;
		    }

		    if (this.className) {
		      	attributes['class'] = this.className;
		    }

		    // Add `data-view` attribute with view key.
		    // For now, view key is same as template.
		    this._data_view = this.id + '_' + this._ts
		    attributes['data-view'] = this._data_view;

		    // Add model & collection meta data from options,
		    // as well as any non-object option values.
		    _.each(this.options, function(value, key) {
		        if (value != null) {
			        if (key === 'model') {
			          	key = 'model_id';
			          	var id = value[value.idAttribute];
			          	if (id == null) {
			            	// Bail if there's no ID; someone's using `this.model` in a
			            	// non-standard way, and that's okay.
			            	return;
			          	}
			            // Cast the `id` attribute to string to ensure it's included in attributes.
			            // On the server, it can be i.e. an `ObjectId` from Mongoose.
			            value = id.toString();
			        } else if (key === 'collection') {
			            key = 'collection_params';
			            value = _.escape(JSON.stringify(value.params));
			        }

			        if (!_.isObject(value) && !_.include(this.nonAttributeOptions, key)) {
			            attributes["data-" + key] = _.escape(value);
			        }
		        }
		    });
		    return attributes;
		},
		getTemplate : function ( ) {
			return __tmp[ this.getTemplateName( ) ];
		},
		getTemplateData: function ( ){
			var payload = this.collection || this.model;
			return payload ? payload.toJSON() : {};
		},
		getTemplateName: function ( ) {
			return this.template || this.id;
		},
		getInnerHtml: function() {
		    var data, template;

		    data = this.getTemplateData();
		    template = this.getTemplate();

		    if (template == null) {
		      	throw new Error(this.name + ": template \"" + this.getTemplateName() + "\" not found.");
		    }
		    return template(data);
		},

		attach: function ( ) {
			if( !this._data_view ) return;
			var $el = $( '[data-view="' + this._data_view + '"]' );
			this.setElement( $el[0] );
			this.$el.attr( 'data-attached', true );
		},

		getHtml: function() {
		    var attrString, attributes, html;

		    html = this.getInnerHtml();
		    attributes = this.getAttributes();
		    attrString = _.inject(attributes, function(memo, value, key) {
		      	return memo += " " + key + "=\"" + value + "\"";
		    }, '');
		    return "<" + this.tagName + attrString + ">" + html + "</" + this.tagName + ">";
		},
	})
);