app.View({
	className : "landing_page",
	id : "landing",
    events : {
//        'click #nav li' : 'navState',
//        'click #logo' : 'resetState'
    },

    navState : function (e) {
        var _this = this;

        $(e.target).addClass('active').siblings().removeClass('active');
    },

    resetState : function () {
        $('#nav li').removeClass('active');
    }
});
