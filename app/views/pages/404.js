app.View({
	className : "404_page",
	tagName : "div",
	id : '404',
	events : {
		'click h1' : 'titleClick'
	},
	titleClick : function ( ) {
		console.log('titleCLick from h1 in subview');
	},
	postRender : function ( ) {
		console.log( 'getAttributes', this.getAttributes( ) );
		console.log( 'getTemplate', this.getTemplate( ) );
		console.log( 'getTemplateData', this.getTemplateData( ) );
		console.log( 'getInnerHtml', this.getInnerHtml( ) );
		console.log( 'getHtml', this.getHtml( ) );
	}
});
