(function(){

	"use stict";

	var AppView = Backbone.View.extend({
		el : "#app",
		initialize : function(){
			//we have this now
			this.$body = $( 'body' );
			this.actives = [];
		}, 
		_errorHandler : function( view, err ){
			// handle errors in view
		},
		_whatPayload : function( view, payload ){
			// setting the data depending on payload
			if( payload instanceof Backbone.Model){
				payload.set( { app : app.toJSON() } );
				view.model = payload;
			}else if( payload instanceof Backbone.Collection ){
				view.collection = payload;
			}
		},
		// this sets active states on links ~ probably should be refactored to handle only nav
		setState : function(){

			var 
			$links = this.$header.find('a'),
			_this = this,
			fragments = Backbone.history.fragment.split(/\//);
			
			_.each( this.actives, function( $el ){
				console.log("el", $el)
				$el.removeClass('active');
			});

			this.actives.length = 0;

	
			$links.each(function(){
				var 
				frags = this.href.
					replace( _this.origin , '').
					split(/\//),
				good = 0,
				$el = $(this);


				frags.shift(1);

				_.each( frags, function( frag, index ){
					if( 
						fragments[ index ] &&
						frag === fragments[ index ] 
					){
						good = good + 1;
					}
				})
				if( good === frags.length ){
					var $li = $el.closest('li');
					$li.
						addClass('active');
					_this.actives.push($li);

				}
			});

		},
		render : function( name, payload, options ){

			payload = payload || {};

			if ( name ) { // avoid 404 everytime
				
				var View = app.getView( name );
				if ( View ) {

					var view = new View( options || {} );

					// if we have a payload set it
					if ( payload ) {
						this._whatPayload( view, payload )
					}

					view.render();
					// render data to dom
					this.$el.html( view.$el );
					this.attachAll( view );
					
					if( this.postRender ){
						this.postRender();
					}

					app.Router.trigger('done');
				
				}


			}
		},
		attachAll : function ( _view ) {
			_.forEach( _view.childViews, function ( view ) {
				if( 
					typeof view === 'object' && 
					typeof view.attach === 'function' 
				){
					view.attach();
				}
			});
		}

	});

	_.extend( app, new AppView({}) )

}());