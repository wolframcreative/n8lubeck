var app = app || {};

(function(){

	"use strict";

	var Workspace = Backbone.Router.extend({
		routes: {
			'' : 'landing',
			'404' : 'notFound'


		},

        default : function () {
          console.log('default');
        },
		loading : function(){
			app.Router.trigger('loading');
		},

		notFound : function( param ){
			this.loading();
			app.render('404')
		},
		landing : function ( param ){
			this.loading();
			app.render('landing');
		}

	});

	app.Workspace = Workspace;

}());