function validateEmail (email) {
    var regular_expression = /\S+@\S+\.\S+/;
    return regular_expression.test(email);
}

function validateTel (tel) {
    var regular_expression = /^[0-9\-\(\)\s]*$/;
    return regular_expression.test(tel)  && tel.replace(/[^0-9]/g,'').length == 10;
}

//Handle form



//    $.ajax({
//		url: '/api/user/signup/',
//		data: text_data,
//		method: 'POST',
//		contentType: 'application/x-www-form-urlencoded',
//		complete: function (s) {
//            console.log(s);
//			$.ajax({
//				url: '/api/sendtext',
//				data: text_data,
//				method: 'POST',
//				success: function (s) {
//					console.log("success", s);
//				},
//				error: function (e) {
//					console.log('error', e);
//				}
//			})
//		}
//	});




$('#ss-signup').click(function(){
    var $button = $(this),
        $form = $button.closest('#ss-form'),
        data = {};

    $('input', $form).each(function(){
        var $input = $(this),
            name = $input.attr('name'),
            val = $input.val();

            data[name] = val;

            console.log(data);
    });
    $.ajax({
        url : '/api/signup',
        data : data,
        method : 'POST',
        contentType : 'application/x-www-form-urlencoded',
        error : function (e) {
            console.log(e, "error");
        },

        success : function (s) {
            $button.append('Thanks');
            console.log(s, "success");
        }
    });

});

$('#login').on('blur', 'input', function(k){
    var $input = $(k.target);
    if ($input.val() === ''){
        $input.addClass('failure');

    } else {
        $input.removeClass('failure');
    }
});

$('#login').on('click', 'input[type=submit]', function(e){
    var fields = $('#login').find('input');

    if (fields.hasClass() === 'failure') {
        alert('Fill in fields');
    }


});
